from pyramid.view import view_config
from .models import *
from .BaseController import *
import os, time
import schedule
import datetime 
import pytz

class EDEN(BaseController):

	def __init__(self, request):
		self.request = request

	@view_config(route_name='home', renderer='../templates/home.jinja2')
	def home(request):
	    return {'project': 'EDEN'}

	@view_config(route_name='dashboard', renderer='../templates/dashboard.jinja2')
	def dashboard(request):
	    return {'project': 'EDEN'}

	def main_app(self):
		if 'currentUser' not in self.sessionValues():
			return self.redirect('/')
		return{"currentUser":self.getSession('currentUser')}

	@view_config(route_name = 'ajax',
			match_param = 'action=login',
			renderer = 'json')
	def login(self):
			email = self.paramValues('email')
			password = self.paramValues('password')
			print(email)
			print(password)
			user = Users.objects(email = email, password = password).first()
			if user:
				self.setSession('currentUser', user)
				return self.returnSuccess()
			else:
				return self.returnError('Invalid Username or Password')


	@view_config(route_name = 'ajax',
			match_param = 'action=register',
			renderer = 'json')

	def register(self):
			name = self.paramValues('name')
			contactNo = self.paramValues('contactNo')
			email = self.paramValues('email')
			password = self.paramValues('password')
			password2 = self.paramValues('password2')
			
			# query the email if it exists
			is_exist = Users.objects(email = email).first()
			if is_exist:
				return self.returnError('Email already in used!')


			# check if password and confirm password is correct
			if password != password2:
				return self.returnError('Password confirmation does not match!')

			user = Users(name = name,
			contactNo = contactNo,
			email = email,
			password = password)
			user.save()
			return self.returnSuccess()


	@view_config(route_name = 'ajax',
			match_param = 'action=lightSwitch',
			renderer = 'json')

	def lightSwitch(self):
		light1 = self.paramValues('light1')	
		lights = lightA.objects()
		for lightz in lights:
			lightz.update(light = light1)
		return self.returnSuccess()


	@view_config(route_name = 'ajax',
			match_param = 'action=timerPotA',
			renderer = 'json')

	def timerPotA(self):
		now_utc = datetime.datetime.utcnow().replace(tzinfo=pytz.utc)
		ph_tz = pytz.timezone('Asia/Taipei')
		t1 = ph_tz.normalize(now_utc.astimezone(ph_tz))
		waHR = int(self.paramValues('waHR'))	
		waMN = int(self.paramValues('waMN'))	
		waWa = int(self.paramValues('waWa'))	
		vol1 = int(self.paramValues('volume'))
		valDay = int(self.paramValues('valDay'))

		if waWa == 0 and waHR == 12:
			waHR = 0

		if waWa == 1:
			waHR = waHR + 12

		t1Hr = int(t1.hour)
		t1Min = int(t1.minute)
		t1Day = int(t1.weekday())
		t1Sec = int(t1.second)

		if waHR >= t1Hr:
			if waMN > t1Min: 
				t1Hrs = t1Hr * 60 * 60
				t1Mins = t1Min * 60  
				t1Days = t1Day * 24 * 60 * 60 
				t1Total = t1Days + t1Hrs + t1Mins + t1Sec

				ms = waMN * 60
				hrs = waHR * 60 * 60
				days = valDay * 24 * 60 * 60
				NewTotal = ms + hrs +days


				def waterA():	
					waters11 = water1.objects()
					for waterz11 in water11s:
						waterz11.update(value =1, volumeInitial = vol1)
				
				def waterB():
					waters22 = water1.objects()
					for waterz22 in waters22:
						waterz22.update(value =0, volumeInitial = 0)

				TotSec = NewTotal - t1Total
				time.sleep(TotSec)
				waterA()
				time.sleep(1)
				waterB()
			else:
				return self.returnError('Scheduled Time Must be Greater than Current Time!')
		else:
			return self.returnError('Scheduled Time Must be Greater than Current Time!')


	@view_config(route_name = 'ajax',
			match_param = 'action=timerPotB',
			renderer = 'json')

	def timerPotB(self):
		now_utc = datetime.datetime.utcnow().replace(tzinfo=pytz.utc)
		ph_tz = pytz.timezone('Asia/Taipei')
		t2 = ph_tz.normalize(now_utc.astimezone(ph_tz))
		waHR2 = int(self.paramValues('waHR3'))	
		waMN2 = int(self.paramValues('waMN3'))	
		waWa2 = int(self.paramValues('waWa3'))	
		vol5 = int(self.paramValues('volume2'))
		valDay2 = int(self.paramValues('valDay2'))

		if waWa2 == 0 and waHR2 == 12:
			waHR2 = 0

		if waWa2 == 1:
			waHR2 = waHR2 + 12

		t2Hr = int(t2.hour)
		t2Min = int(t2.minute)
		t2Day = int(t2.weekday())
		t2Sec = int(t2.second)

		if waHR2 >= t2Hr:
			if waMN2 > t2Min: 
				t2Hrs = t2Hr * 60 * 60
				t2Mins = t2Min * 60  
				t2Days = t2Day * 24 * 60 * 60 
				t2Total = t2Days + t2Hrs + t2Mins + t2Sec

				ms1 = waMN2 * 60
				hrs1 = waHR2 * 60 * 60
				days1 = valDay2 * 24 * 60 * 60
				NewTotal1 = ms1 + hrs1 +days1


				def waterC():	
					waters33 = water2.objects()
					for waterz33 in waters33:
						waterz33.update(value =1, volumeInitial = vol5)
				
				def waterD():
					waters44 = water2.objects()
					for waterz44 in waters44:
						waterz44.update(value =0, volumeInitial = 0)

				TotSec1 = NewTotal1 - t2Total
				time.sleep(TotSec1)
				waterC()
				time.sleep(1)
				waterD()
			else:
				return self.returnError('Scheduled Time Must be Greater than Current Time!')
		else:
			return self.returnError('Scheduled Time Must be Greater than Current Time!')


	@view_config(route_name = 'ajax',
			match_param = 'action=timerPotC',
			renderer = 'json')

	def timerPotC(self):
		now_utc = datetime.datetime.utcnow().replace(tzinfo=pytz.utc)
		ph_tz = pytz.timezone('Asia/Taipei')
		t3 = ph_tz.normalize(now_utc.astimezone(ph_tz))
		waHR8 = int(self.paramValues('waHR8'))	
		waMN8 = int(self.paramValues('waMN8'))	
		waWa8 = int(self.paramValues('waWa8'))	
		vol7 = int(self.paramValues('volume7'))
		valDay7 = int(self.paramValues('valDay7'))

		if waWa8 == 0 and waHR8 == 12:
			waHR8 = 0

		if waWa8 == 1:
			waHR8 = waHR8 + 12

		print(waHR8)

		t3Hr = int(t3.hour)
		t3Min = int(t3.minute)
		t3Day = int(t3.weekday())
		t3Sec = int(t3.second)
		print(t3Hr)

		if waHR8 >= t3Hr:
			if waMN8 > t3Min: 
				t3Hrs = t3Hr * 60 * 60
				t3Mins = t3Min * 60  
				t3Days = t3Day * 24 * 60 * 60 
				t3Total = t3Days + t3Hrs + t3Mins + t3Sec

				ms8 = waMN8 * 60
				hrs8 = waHR8 * 60 * 60
				days8 = valDay7 * 24 * 60 * 60
				NewTotal8 = ms8 + hrs8 +days8


				def waterE():	
					waters55 = water3.objects()
					for waterz55 in waters55:
						waterz55.update(value =1, volumeInitial = vol7)
				
				def waterF():
					waters66 = water3.objects()
					for waterz66 in waters66:
						waterz66.update(value =0, volumeInitial = 0)
				
				TotSec8 = NewTotal8 - t3Total
				time.sleep(TotSec8)
				waterE()
				time.sleep(1)
				waterF()
			else:
				return self.returnError('Scheduled Time Must be Greater than Current Time!')
		else:
			return self.returnError('Scheduled Time Must be Greater than Current Time!')


	@view_config(route_name = 'potA', renderer = 'json')
	def potA(request):
		showPotA = Pot1.objects()

		return {"pot_dataA":showPotA.to_json()}

	@view_config(route_name = 'potB', renderer = 'json')
	def potB(request):
		showPotB = Pot2.objects()

		return {"pot_dataB":showPotB.to_json()}

	@view_config(route_name = 'potC', renderer = 'json')
	def potC(request):
		showPotC = Pot3.objects()

		return {"pot_dataC":showPotC.to_json()}


	@view_config(route_name = 'ajax',
			match_param = 'action=waterNow1',
			renderer = 'json')

	def waterNow1(self):
		volInitial = self.paramValues('waterVolA')	
		waterb1 = water1.objects()
		for watera1 in waterb1:
			watera1.update(value = 1, volumeInitial = volInitial)
		time.sleep(1)
		watera2 = water1.objects()
		for waterb2 in watera2:
			waterb2.update(value = 0, volumeInitial = 0)
		return self.returnSuccess()

	@view_config(route_name = 'ajax',
			match_param = 'action=waterNow2',
			renderer = 'json')

	def waterNow2(self):
		volInitial = self.paramValues('waterVolB')	
		waterr1 = water2.objects()
		for watert1 in waterr1:
			watert1.update(value = 1, volumeInitial = volInitial)
		time.sleep(1)
		watert2 = water2.objects()
		for waterr2 in watert2:
			waterr2.update(value = 0, volumeInitial = 0)
		return self.returnSuccess()

	@view_config(route_name = 'ajax',
			match_param = 'action=waterNow3',
			renderer = 'json')

	def waterNow3(self):
		volInitial = self.paramValues('waterVolC')	
		waterx3 = water3.objects()
		for waterc1 in waterx1:
			waterc1.update(value = 1, volumeInitial = volInitial)
		time.sleep(1)
		waterv2 = water3.objects()
		for watern2 in waterv2:
			watern2.update(value = 0, volumeInitial = 0)
		return self.returnSuccess()


	@view_config(route_name = 'ajax',
			match_param = 'action=timerLightA',
			renderer = 'json')

	def timerLightA(self):
		now_utc = datetime.datetime.utcnow().replace(tzinfo=pytz.utc)
		ph_tz = pytz.timezone('Asia/Taipei')
		t1 = ph_tz.normalize(now_utc.astimezone(ph_tz))
		startHr = int(self.paramValues('strHr'))	
		startMin = int(self.paramValues('strMin'))
		startLyt = int(self.paramValues('strLyt'))
		endHr = int(self.paramValues('endHr'))
		endMin = int(self.paramValues('endMin'))
		endLyt = int(self.paramValues('endLyt'))
		day = int(self.paramValues('dayA'))	
		# y = lightA.objects(light__size)

		if startLyt == 0 and startHr == 12:
			startHr = 0

		if endLyt == 0 and endHr == 12:
			endHr = 0

		if startLyt == 1:
			startHr = startHr + 12

		if endLyt == 1:
			endHr = endHr + 12

		t1Hr = int(t1.hour)
		t1Min = int(t1.minute)
		t1Day = int(t1.weekday())
		t1Sec = int(t1.second)


		if startHr >= t1Hr:
			if startMin > t1Min: 
				t1Hrs = t1Hr * 60 * 60
				t1Mins = t1Min * 60  
				t1Days = t1Day * 24 * 60 * 60 
				t1Total = t1Days + t1Hrs + t1Mins + t1Sec
				if endHr >= startHr:
					if endMin > startMin: 
						ems = endMin * 60
						ehrs = endHr * 60 * 60
						ms = startMin * 60
						hrs = startHr * 60 * 60
						days = day * 24 * 60 * 60
						NewTotal = ms + hrs +days
						LastTotal = ems + ehrs +days

						def lightOff():
							lights = lightA.objects()
							for lightz in lights:
								lightz.update(light = 0)
							return lightOff

						def lightOn():
							lights = lightA.objects()
							for lightz in lights:
								lightz.update(light = 1)
							return lightOn

						TotSec = NewTotal - t1Total
						time.sleep(TotSec)
						lightOn()
						FinSec = LastTotal - NewTotal
						time.sleep(FinSec)
						lightOff()
					else:
						return self.returnError('Scheduled Time Must be Greater than Current Time!')
				else:
					return self.returnError('Scheduled Time Must be Greater than Current Time!')
			else:
				return self.returnError('Scheduled Time Must be Greater than Current Time!')
		else:
			return self.returnError('Scheduled Time Must be Greater than Current Time!')



	@view_config(route_name = 'ajax',
			match_param = 'action=camCheck',
			renderer = 'json')

	def camCheck(self):
		val = self.paramValues('cam')
		camCheck1 = cam.objects()
		for check1 in camCheck1:
			check1.update(value = 1)
		time.sleep(1)
		camCheck2 = cam.objects()
		for check2 in camCheck2:
			check2.update(value = 0)
		return self.returnSuccess()