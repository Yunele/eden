
# # External module imp
# from http.server import BaseHTTPRequestHandler, HTTPServer
# import RPi.GPIO as GPIO
# import datetime
# import time

# init = False
# Request = None

# GPIO.setmode(GPIO.BOARD) # Broadcom pin-numbering scheme

# def get_last_watered():
#     try:
#         f = open("last_watered.txt", "r")
#         return f.readline()
#     except:
#         return "NEVER!"
      
# def get_status(pin = 8):
#     GPIO.setup(pin, GPIO.IN) 
#     return GPIO.input(pin)

# def init_output(pin):
#     GPIO.setup(pin, GPIO.OUT)
#     GPIO.output(pin, GPIO.LOW)
#     GPIO.output(pin, GPIO.HIGH)
    
# def auto_water(delay = 5, pump_pin = 7, water_sensor_pin = 8):
#     consecutive_water_count = 0
#     init_output(pump_pin)
#     print("Here we go! Press CTRL+C to exit")
#     try:
#         while 1 and consecutive_water_count < 10:
#             time.sleep(delay)
#             wet = get_status(pin = water_sensor_pin) == 0
#             if not wet:
#                 if consecutive_water_count < 5:
#                     pump_on(pump_pin, 1)
#                 consecutive_water_count += 1
#             else:
#                 consecutive_water_count = 0
#     except KeyboardInterrupt: # If CTRL+C is pressed, exit cleanly:
#         GPIO.cleanup() # cleanup all GPI

# def pump_on1(pump_pin = 7, delay = 5):
#     init_output(pump_pin)
#     f = open("last_watered.txt", "w")
#     f.write("Last watered {}".format(datetime.datetime.now()))
#     f.close()
#     GPIO.output(pump_pin, GPIO.LOW)
#     time.sleep(1)
#     GPIO.output(pump_pin, GPIO.HIGH)

# def pump_on2(pump_pin = 11, delay = 5):
#     init_output(pump_pin)
#     f = open("last_watered.txt", "w")
#     f.write("Last watered {}".format(datetime.datetime.now()))
#     f.close()
#     GPIO.output(pump_pin, GPIO.LOW)
#     time.sleep(10)
#     GPIO.output(pump_pin, GPIO.HIGH)

# def pump_on3(pump_pin = 13, delay = 5):
#     init_output(pump_pin)
#     f = open("last_watered.txt", "w")
#     f.write("Last watered {}".format(datetime.datetime.now()))
#     f.close()
#     GPIO.output(pump_pin, GPIO.LOW)
#     time.sleep(1)
#     GPIO.output(pump_pin, GPIO.HIGH)

# class RequestHandler_httpd(BaseHTTPRequestHandler):
#     def do_GET(self):
#         global Request
#         messagetosend = bytes('Hello World!',"utf")
#         self.send_response(200)
#         self.send_header('Content-Type', 'text/plain')
#         self.send_header('Content-Length', len(messagetosend))
#         self.end_headers()
#         self.wfile.write(messagetosend)
#         Request = self.requestline
#         Request = Request[5:int(len(Request))-9]
#         print(Request)
#         if Request == 'pump1':
#             pump_on1()
#         if Request == 'pump2':
#             pump_on2()
#         return



# server_address_httpd = ('192.168.43.208', 1234)    
# httpd = HTTPServer(server_address_httpd, RequestHandler_httpd)
# print('Starting server:')
# httpd.serve_forever()   