import os
from pyramid.httpexceptions import HTTPFound

class BaseController:
	def __init__(self, request):
		self.request = request

	def paramValues(self, key = None):
		if key is None:
			return self.request.params
		else:
			return self.request.params[key]

	def returnError(self, message = None):
		if message is not None:
			return {'success': False, 'message': message}
		else:
			return {'success': False}

	def returnSuccess(self, message = None):
		if message is not None:
			return {'success': True, 'message': message}
		else:
			return {'success': True}

	def setSession(self, key, value):
		self.request.session[key] = value

	def getSession(self, key):
		if key not in self.request.session:
			return None
		else:
			return self.request.session[key]

	def sessionValues(self):
		return self.request.session

	def redirect(self, url):
		return HTTPFound(url)

	def deleteSession(self, key):
		if key in self.request.session:
			del self.request.session[key]