from pyramid.config import Configurator
from pyramid.session import SignedCookieSessionFactory

session_factory = SignedCookieSessionFactory('eden', max_age = 43200, timeout = 6500)

def main(global_config, **settings):
# This function returns a Pyramid WSGI application.

	with Configurator(settings=settings) as config:
		config.set_session_factory(session_factory)
		config.include('pyramid_jinja2')
		config.include('.routes')
		config.scan()
	return config.make_wsgi_app()