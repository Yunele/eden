def includeme(config):
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_route('home', '/')
    config.add_route('ajax', 'ajax/{action}')
    config.add_route('dashboard', 'dashboard')
    config.add_route('potA', 'potA')
    config.add_route('potB', 'potB')
    config.add_route('potC', 'potC')